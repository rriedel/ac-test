from __future__ import print_function # In python 2.7
import sys

import json
from flask import jsonify, request
from flask_restful import Resource, reqparse
from models import Product, Image, db
import base64

class ProductChildList(Resource):
    def get(self, product_id):
        Product.query.get_or_404(product_id)

        child_list = Product.query.filter_by(parent_product_id=product_id).all()

        return jsonify([p.as_dict() for p in child_list])


class ImageList(Resource):
    def get(self, product_id):
        parent_p = Product.query.get_or_404(product_id)

        i_l = Image.query.filter_by(parent_product_id=product_id).all()

        result = []
        for i in i_l:
            result.append({'image_id': i.image_id, 'parent_product_id': i.parent_product_id, 'type': i.image_type, 'content': i.content})

        return jsonify(result)

    def post(self, product_id):
        Product.query.get_or_404(product_id)
        parser = reqparse.RequestParser()

        parser.add_argument('type', type=unicode)
        parser.add_argument('file')

        args = parser.parse_args()
        file = args['file']#base64.b64decode(args['file'].split(',')[1])

        p = Image(args['type'], file, product_id)

        db.session.add(p)
        db.session.commit()

        return 'Image successifully stored', 201


class ImageView(Resource):
    def get(self, product_id, image_id):
        Product.query.get_or_404(product_id)
        i = Image.query.get_or_404(image_id)

        r = i.as_dict()


        return jsonify(r)

    def delete(self, product_id, image_id):
        Product.query.get_or_404(product_id)
        i = Image.query.get_or_404(image_id)

        db.session.delete(i)
        db.session.commit()

        return 'Image deleted successifully', 200




class ProductView(Resource):
    def get(self, product_id):
        p = Product.query.get_or_404(product_id)

        parser = reqparse.RequestParser()
        parser.add_argument('expand', type=str)

        args = parser.parse_args()

        expand = []
        if args['expand']:
            expand = args['expand'].split(',')

        r = p.as_dict()

        if 'product' in expand:
            r['products'] = [ x.as_dict() for x in Product.query.filter_by(parent_product_id=p.product_id)]

        if 'image' in expand:
            r['images'] = [ x.as_dict() for x in Image.query.filter_by(parent_product_id=p.product_id)]


        return jsonify(r)

    def delete(self, product_id):
        p = Product.query.get_or_404(product_id)

        db.session.delete(p)
        db.session.commit()

        return 'Product deleted successifully', 200

    def put(self, product_id):
        p = Product.query.get_or_404(product_id)

        parser = reqparse.RequestParser()

        parser.add_argument('name', type=unicode)
        parser.add_argument('description', type=unicode)
        parser.add_argument('parent_product_id', type=int)

        args = parser.parse_args()

        if args['parent_product_id']:
            parent_p = Product.query.get_or_404(args['parent_product_id'])

        p.name = args['name']
        p.description = args['description']
        p.parent_product_id = args['parent_product_id']
        db.session.commit()

        return 'Product updated successifully', 200



class ProductList(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('expand', type=str)

        args = parser.parse_args()

        expand = []
        if args['expand']:
            expand = args['expand'].split(',')

        r = []
        for p in Product.query.all():
            i = p.as_dict()

            if 'product' in expand:
                i['products'] = [ x.as_dict() for x in Product.query.filter_by(parent_product_id=p.product_id)]

            if 'image' in expand:
                i['images'] = [ x.as_dict() for x in Image.query.filter_by(parent_product_id=p.product_id)]

            r.append(i)

        return jsonify(r)

    def post(self, *args, **kwargs):
        parser = reqparse.RequestParser()

        parser.add_argument('name', type=unicode)
        parser.add_argument('description', type=unicode)
        parser.add_argument('parent_product_id', type=int)

        args = parser.parse_args()
        if args['parent_product_id']:
            parent_p = Product.query.get_or_404(args['parent_product_id'])

        p = Product(args['name'], args['description'], args['parent_product_id'])
        db.session.add(p)
        db.session.commit()

        return 'Product successifully created', 201