var app = angular.module('poc', ['ui.router', 'ngMaterial', 'ngResource', 'lfNgMdFileInput']);

app.config(function($stateProvider, $urlRouterProvider){
    // configurando os estados
    $stateProvider
        .state('doAddNewProduct', {
            url: "/addNewProduct",
            templateUrl: "/static/partials/product-add.html",
            controller: "ProductAddController",
        })
        .state('doProductEdit', {
            url: "/editProduct/:product_id/",
            templateUrl: "/static/partials/product-edit.html",
            controller: "ProductEditController",
        })
        .state('doProductListing', {
            url: "/productListing",
            templateUrl: "/static/partials/main-product-list.html",
            controller: "ProductListController"
        })
        .state('doProductImageListing', {
            url: "/editProduct/:product_id/images",
            templateUrl: "/static/partials/product-image-list.html",
            controller: "ImageListController"
        })
        .state('doProductImageAdd', {
            url: "/editProduct/:product_id/images/add",
            templateUrl: "/static/partials/product-image-add.html",
            controller: "ImageAddController"
        })
        .state('doAddChildProduct', {
            url: "/editProduct/:product_id/addChildProduct",
            templateUrl: "/static/partials/product-add-child.html",
            controller: "ProductAddController",
        })
    })


.factory('Product', ['$resource', function($resource) {
    return $resource('/product/:product_id', {product_id: '@product_id'}, {
        update: {
            method: 'PUT'
        },
        getChild: {
            method: 'GET',
            url: '/product/:product_id/products',
            isArray: true
        }
    });
}])


.factory('Image', ['$resource', function($resource) {
    return $resource('/product/:product_id/image/:image_id', {product_id: '@parent_product_id', image_id: '@image_id'});
}]);


app.controller('ProductListController', ['$scope', '$state', '$mdDialog', 'Product',
    function($scope, $state, $mdDialog, Product) {

        $scope.products = new Product.query();

        $scope.deleteProduct = function(product) {

           var confirm = $mdDialog.confirm()
                  .title('Would you like to delete the product "' + product.name + '"?')
                  .textContent('The product will be wiped out and there is no return.')
                  .ariaLabel('Delete product')
                  .ok('Confirm')
                  .cancel('Cancel');

            $mdDialog.show(confirm).then(function(){
                product.$delete(function() {
                    $state.reload();
                });
            }, function() {
                // pass
            });
        };
    }
]);

app.controller('ImageListController', ['$scope', '$state', '$mdDialog', '$stateParams','Image', 'Product',
    function($scope, $state, $mdDialog,  $stateParams, Image, Product) {

        $scope.product = Product.get({product_id: $stateParams.product_id});
        $scope.images = new Image.query({product_id: $stateParams.product_id});

        $scope.deleteImage = function(data) {
            console.log(data);
            $scope.image = Image.get({product_id: $stateParams.product_id, image_id: data.image_id});
            
            var confirm = $mdDialog.confirm()
                  .title('Would you like to delete this image?')
                  .ariaLabel('Delete image')
                  .ok('Confirm')
                  .cancel('Cancel');

            $mdDialog.show(confirm).then(function(){
                $scope.image.$delete(function() {
                    $state.reload();
                });
                
            }, function() {
                // pass
            });

        };
    }
]);

app.controller('ImageAddController', ['$scope', '$stateParams', '$state','Image',
    function($scope, $stateParams, $state, Image) {
        $scope.image = new Image();

        $scope.$watch('files.length',function(newVal,oldVal){
            console.log($scope.files);
        });


        $scope.onFileClick = function(obj,idx){
            console.log(obj);
        };
        $scope.onSubmitClick = function(files) {
            console.log(files);
        }
        $scope.onSubmitFilesChange = function() {
            console.log($scope.submitButtonFiles);
        }
        $scope.onFileRemove = function(obj,idx){
            console.log(obj);
        };

        $scope.addProductImage = function(){
            var reader = new FileReader();
            

            console.log("addProductImage.$scope.files", $scope.files[0].lfFile);
            f = $scope.files[0].lfFile;

            reader.onload = (function(theFile) {
                return function(e) {
                    //console.debug(e.target.result);
                    $scope.image.file = e.target.result;
                    $scope.image.parent_product_id = $stateParams.product_id;
                    $scope.image.$save(function(){
                        $state.go("doProductImageListing", {product_id: $stateParams.product_id});
                    });
                };
            })(f);

            reader.readAsDataURL(f);
            
        };

        $scope.addProduct = function() {
            $scope.product.$save(function(){
                $state.go("doProductListing");
            })
        }
    }
]);


app.controller('ProductEditController', ['$scope', '$stateParams', '$mdDialog', '$state', 'Product',
    function($scope, $stateParams, $mdDialog, $state, Product) {
        $scope.product = Product.get({ product_id: $stateParams.product_id});
        $scope.parent_products = [];
        var product_list = new Product.query(function(){            
            angular.forEach(product_list, function(item){
                if(item.product_id != $stateParams.product_id)
                    $scope.parent_products.push(item);
            });
        });
        
        

        $scope.child_products = Product.getChild({ product_id: $stateParams.product_id});

        $scope.addProduct = function(data) {
            console.log("ProductEditController.addProduct", data);
            $scope.product.$save(function(){
                $state.go("doProductListing");
            })
        }

        $scope.updateProduct = function() {
            $scope.product.$update(function(){
                $state.go("doProductListing");
            })
        };

        $scope.showImages = function(product) {
            console.log(product);
            $scope.product = product;
            $state.go("doProductImageListing", {product_id: product.product_id});
        };

        $scope.deleteProduct = function(product) {
            $scope.child_product = Product.get({ product_id: product.product_id});

            var confirm = $mdDialog.confirm()
                  .title('Would you like to unlink the product "' + product.name + '"?')
                  .textContent('The product will be no longer child from the current one.')
                  .ariaLabel('Unlink product')
                  .ok('Confirm')
                  .cancel('Cancel');

            $mdDialog.show(confirm).then(function(){
                $scope.child_product.parent_product_id = null;
                $scope.child_product.$update(function(){
                    $state.reload();
                })
            }, function() {
                // pass
            });
        }
    }
]);

app.controller('ProductAddController', ['$scope', '$stateParams', '$state', 'Product',
    function($scope, $stateParams, $state, Product) {
        $scope.product = new Product();
        $scope.parent_products = new Product.query();

        if($stateParams.product_id) {
            console.log("stateParams.product_id", $stateParams.product_id);
            $scope.parent_product = Product.get({ product_id: $stateParams.product_id});
        }

        $scope.addChildProduct = function() {
            $scope.product.parent_product_id = $stateParams.product_id;
            $scope.addProduct();
        }

        $scope.addProduct = function() {
            $scope.product.$save(function(){
                $state.go("doProductListing");
            })
        }
    }
]);