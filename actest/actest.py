from flask import Flask, g, render_template
from flask_restful import  Api, Resource
from flask_sqlalchemy import SQLAlchemy


def create_app():
    app = Flask(__name__)

    app.config.update(dict(
        SQLALCHEMY_DATABASE_URI='sqlite://',
        SQLALCHEMY_TRACK_MODIFICATIONS=False
    ))
    app.config.from_object(__name__)
    app.config.from_envvar('ACTEST_SETTINGS', silent=True)
    app.url_map.strict_slashes = False

    api = Api(app)

    from models import Product, db

    db.init_app(app)

    with app.app_context():
        db.create_all()


    class Home(Resource):
        def get(self):
            return "hello"

    from views import ProductView, ProductList, ImageList, ProductChildList, ImageView

    #api.add_resource(Home, '/')
    api.add_resource(ProductList, '/product')
    api.add_resource(ProductView, '/product/<int:product_id>')

    #Get set of images for specific product 
    #Create set of images for specific product
    api.add_resource(ImageList, '/product/<int:product_id>/image')

    api.add_resource(ImageView, '/product/<int:product_id>/image/<int:image_id>')

    # Get set of child products for specific product
    api.add_resource(ProductChildList, '/product/<int:product_id>/products')

    @app.route("/")
    def index():
        return render_template("index.html")


    return app



if __name__ == '__main__':
    app = create_app()
    app.run(port=5000, host='0.0.0.0')
    