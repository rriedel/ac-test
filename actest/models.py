from flask_sqlalchemy import SQLAlchemy
import json
db = SQLAlchemy()

class Product(db.Model):
    product_id = db.Column(db.Integer, primary_key=True)
    parent_product_id = db.Column(db.Integer, db.ForeignKey('product.product_id'))
    name = db.Column(db.String(50), unique=True)
    description = db.Column(db.String(255))
    parent_product = db.relationship('Product', remote_side=[product_id])

    def __init__(self, name, description, parent_product_id=None):
        self.name = name
        self.description = description
        self.parent_product_id = parent_product_id

    def __repr__(self):
        return '<Product %s:%r>' % (self.product_id, self.name)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}
"""
    def __dict__(self):
        return {'id': self.id, 'name': self.name, 'description': self.description}
"""
class Image(db.Model):
    image_id = db.Column(db.Integer, primary_key=True)
    image_type = db.Column(db.String(25))
    content = db.Column(db.LargeBinary)
    parent_product_id = db.Column(db.Integer, db.ForeignKey('product.product_id'))
    parent_product = db.relationship('Product')

    def __init__(self, image_type, content, parent_product_id):
        self.image_type = image_type
        self.content = content
        self.parent_product_id = parent_product_id

    def __repr__(self):
        return '<Image %s:%s>' % (self.image_id, self.image_type)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}