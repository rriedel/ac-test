FROM python:2

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# installing Node.js
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - ; \
    apt-get install -y nodejs

COPY . .

# running npm
RUN cd /usr/src/app/actest/static ; \
    npm install


EXPOSE 5000

CMD [ "python", "./actest/actest.py"]