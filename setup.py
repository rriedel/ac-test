from setuptools import setup

setup(
    name='actest',
    packages=['actest'],
    include_package_data=True,
    install_requires=[
        'flask'
    ],
)