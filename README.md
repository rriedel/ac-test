# ac-test

Avenue Code's Recruitment test

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
* Linux

#### Docker way
* Docker

#### Shell way
* Python 2.7
* [VirtualEnv](https://virtualenv.pypa.io/en/stable/)
* Node.js

### Running

There are two options to run this application: docker **(strongly recommend)** or shell

#### Docker
In order to run by using docker, you need docker up and running in your system, and then build and run the container.

```
$ docker build -t ac-test-rriedel .
$ docker run -it -p 5000:5000 --rm --name ac-test-running-app ac-test-rriedel
```
#### Shell
```
$ python actest.py
```

### Running test units

#### Docker Way
With the container running, just execute the following command that will run pytest inside the container.
```
$ docker exec -it ac-test-running-app  pytest
```
#### Shell way
```
$ pytest
```

### Accessing the frontend
Once the application is up and running, you can now access the frontend using the address [http://localhost:5000](http://localhost:5000)
## Built With

### Backend

* [Flask](http://flask.pocoo.org/) - The web framework used
* [flask-restful](https://github.com/flask-restful/flask-restful/) - Flask extension for building REST APIs
* [flask-sqlalchemy](http://flask-sqlalchemy.pocoo.org/2.1/) - Flask extension for SQLAlchemy support
* [SQLAlchemy](https://www.sqlalchemy.org/) - Python SQL Toolkit and Object Relational Mapper
* [SQLite3](https://www.sqlite.org/) - Database used

### Frontend

* [AngularJS](https://angularjs.org/) - Javascript framework
* [AngularJS Material](https://material.angularjs.org/) - Google's Material Design implementation for AngularJS
* [Angular UI-Router](https://github.com/angular-ui/ui-router) - SPA UI Routing framework for AngularJS

## Author

* **Rafael Riedel** - [RRiedel](https://bitbucket.com/rriedel)

## Acknowledgments

* Thanks for the opportunity, although a regular logic test result... >.<