import pytest
import json
from flask import Flask
from actest.actest import create_app



@pytest.fixture(scope='function')
def client():
    app = create_app()

    with app.test_client() as c:
        yield c

@pytest.fixture(scope='module')
def product_data():
    return  dict(
        name=u'Product',
        description=u'product description') 

@pytest.fixture(scope='module')
def product_data_child():
    return  dict(
        name=u'Product CHILD',
        description=u'product description',
        parent_product_id=1) 

@pytest.fixture(scope='module')
def product_data_invalid_parent():
    return  dict(
        name=u'Product XPTO',
        description=u'product description',
        parent_product_id=99) 

@pytest.fixture(scope='function')
def image_data():
    return dict(
        type=u'image type',
        file=u'file content')


def test_get_product_list_with_relationships(client, product_data, product_data_child, image_data):
    test_create_nested_products(client, product_data, product_data_child)

    # creating a imgage
    r = client.post('/product/1/image', data=image_data)

    assert 201 == r.status_code

    # get product list
    r = client.get('/product?expand=product,image')

    assert 200 == r.status_code

    d = json.loads(r.get_data())

    assert d[0]['products'][0]['parent_product_id'] == 1
    assert d[1]['products'] == []

    assert d[0]['images'][0]['parent_product_id'] == 1
    assert d[1]['images'] == []


    # get product
    r = client.get('/product/1?expand=product,image')

    assert 200 == r.status_code

    d = json.loads(r.get_data())

    assert d['products'][0]['parent_product_id'] == 1
    assert d['images'][0]['parent_product_id'] == 1


def test_get_invalid_product_child(client):
    # get child products
    r = client.get('/product/1/products')

    assert 404 == r.status_code


def test_create_invalid_product_image(client, image_data):
    # creating a invalid product image
    r = client.post('/product/1/image', data=image_data)

    assert 404 == r.status_code


def test_get_invalid_product_image(client):
    # get image
    r = client.get('/product/1/image')

    assert 404 == r.status_code


def test_create_product_image(client, product_data, image_data):
    # creating a product
    r = client.post('/product', data=product_data)

    assert 201 == r.status_code

    # creating a imgage
    r = client.post('/product/1/image', data=image_data)

    assert 201 == r.status_code

    # get image
    r = client.get('/product/1/image')

    assert 200 == r.status_code

    d = json.loads(r.get_data())

    assert len(d) == 1

    #d = d[0]
    assert image_data['type'] == d[0]['type']
    assert 1 == d[0]['image_id']
    assert image_data['file'] == d[0]['content']


def test_update_invalid_nested(client, product_data, product_data_invalid_parent):
    r = client.post('/product', data=product_data)

    assert 201 == r.status_code

    # update
    r = client.put('/product/1', data=product_data_invalid_parent)

    d = json.loads(r.get_data())

    assert r.status_code == 404


def test_create_invalid_nested(client, product_data_invalid_parent):
    r = client.post('/product', data=product_data_invalid_parent)

    assert 404 == r.status_code


def test_create_nested_products(client, product_data, product_data_child):
    r = client.post('/product', data=product_data)

    assert 201 == r.status_code
    assert u'Product successifully created' in r.get_data()


    # getting main product
    r = client.get('/product/1')

    d = json.loads(r.get_data())

    assert r.status_code == 200
    assert d['name'] == product_data['name']
    assert d['description'] == product_data['description']
    assert d['parent_product_id'] == None



    r = client.post('/product', data=product_data_child)

    assert 201 == r.status_code
    assert u'Product successifully created' in r.get_data()

    # getting child product
    r = client.get('/product/2')

    assert r.status_code == 200

    d = json.loads(r.get_data())
    
    assert d['name'] == product_data_child['name']
    assert d['description'] == product_data_child['description']
    assert d['parent_product_id'] == 1




    # getting product 1 child
    r = client.get('/product/1/products')

    assert r.status_code == 200

    d = json.loads(r.get_data())
    d = d[0]

    assert d['name'] == product_data_child['name']
    assert d['description'] == product_data_child['description']
    assert d['product_id'] == 2


def test_update_inexistent_object(client, product_data):
    r = client.put('/product/1', data=product_data)

    d = json.loads(r.get_data())

    assert r.status_code == 404


def test_delete_inexistent_object(client):
    r = client.delete('/product/1')

    assert r.status_code == 404


def test_full_product_operation(client, product_data):
    #empty list
    r = client.get('/product')
    
    d = json.loads(r.get_data())


    assert r.status_code == 200
    assert d == []

    r = client.post('/product', data=product_data)

    assert 201 == r.status_code
    assert u'Product successifully created' in r.get_data()

    # getting a product list
    r = client.get('/product')
    
    d = json.loads(r.get_data())
    d = d[0]
    assert r.status_code == 200
    assert d['product_id'] == 1
    assert d['name'] == product_data['name']
    assert d['description'] == product_data['description']


    # getting a product
    r = client.get('/product/1')

    d = json.loads(r.get_data())

    assert r.status_code == 200
    assert d['name'] == product_data['name']
    assert d['description'] == product_data['description']

    
    # update a product
    product_data = dict(
        name=u'product alpha:changed',
        description=u'product description:changed')

    r = client.put('/product/1', data=product_data)

    d = json.loads(r.get_data())

    assert r.status_code == 200
    assert u'Product updated successifully' in r.get_data()
    
    # getting a product changed
    r = client.get('/product/1')

    d = json.loads(r.get_data())

    assert r.status_code == 200
    assert d['name'] == product_data['name']
    assert d['description'] == product_data['description']


    # deleting a product
    r = client.delete('/product/1')

    d = json.loads(r.get_data())

    assert r.status_code == 200


    # getting a product deleted
    r = client.get('/product/1')

    assert r.status_code == 404


